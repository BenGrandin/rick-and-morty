import { VueConstructor } from "vue/types/vue";
import { uppercaseFilter } from "@/filters/uppercase.filter";
import { capitalizeFilter } from "@/filters/capitalize.filter";
import { europeanDateFormat } from "@/filters/date.filter";

interface IFilter {
  [key: string]: (arg: string) => string;
}

export default function registerGlobalFilters(Vue: VueConstructor): void {
  const filters: IFilter = {
    // Declare filters here
    capitalize: capitalizeFilter,
    europeanDateFormat,
    uppercase: uppercaseFilter,
  };

  Object.keys(filters).forEach((name: string) => {
    Vue.filter(name, filters[name]);
  });
}
