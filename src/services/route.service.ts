import { ROUTES } from "@/router/routes";

export function generateCharacterPage(id: string) {
  return ROUTES.character.path.replace(":id", id);
}
