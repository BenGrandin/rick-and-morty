import { ERole } from "@/router/routes";

export function isGranted(
  routeRoles?: ERole[],
  userRoles: ERole[] = []
): boolean {
  return (
    routeRoles === undefined ||
    routeRoles.length === 0 ||
    routeRoles.some((r: ERole) => userRoles.includes(r))
  );
}
