export function capitalizeFilter(value = ""): string {
  if (value.length === 0) {
    return "";
  }
  const [first, ...tail] = Array.from(value.toLowerCase());
  return first.toUpperCase() + tail.join("");
}
