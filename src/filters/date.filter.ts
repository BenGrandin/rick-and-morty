import dayjs from "dayjs";
import LocalizedFormat from "dayjs/plugin/localizedFormat";

dayjs.extend(LocalizedFormat);

function europeanDateFormat(date: string): string {
  return dayjs(date).format("DD/MM/YYYY");
}

export { europeanDateFormat };
