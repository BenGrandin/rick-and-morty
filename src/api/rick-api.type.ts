export interface IRickApiInfo {
  count: number;
  pages: number;
  next: string;
  prev: string | null;
  current?: number;
}

export interface IRickApiResponse<T> {
  info: Partial<IRickApiInfo>;
  results: T[];
}

export enum ERickCharacterSpecies {
  HUMAN = "Human",
  ALIEN = "Alien",
}
