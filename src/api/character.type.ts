export interface ICharacter {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: ICharacterLink;
  location: ICharacterLink;
  created: string;
  image: string;
  episode: string[];
  url: string;
}

export interface ICharacterLink {
  name: string;
  url: string;
}
