import { ICharacter } from "@/api/character.type";
import { IRickApiResponse } from "@/api/rick-api.type";

const RICK_API_BASE_URL = "https://rickandmortyapi.com/api";

export interface ICharacterFilters {
  name: string;
  status: ECharacterStatus;
  species: string;
  type: string;
  gender: ECharacterGender;
  page: number;
}

export enum ECharacterStatus {
  ALIVE = "alive",
  DEAD = "dead",
  UNKNOWN = "unknown",
}

enum ECharacterGender {
  FEMALE = "female,",
  MALE = "male,",
  GENDERLESS = "genderless",
  UNKNOWN = "unknown",
}

export class CharacterApi {
  public static RICK_API_CHARACTERS_URL = RICK_API_BASE_URL + "/character";

  public static async getAllCharacters(
    filters: Partial<ICharacterFilters>
  ): Promise<IRickApiResponse<ICharacter>> {
    let url = this.RICK_API_CHARACTERS_URL;

    if (Object.values(filters).some((v) => v !== "")) {
      const stringFilters = Object.entries(filters)
        .filter(([, value]) => (Array.isArray(value) ? value.length : value))
        .map(([key, value]) => {
          if (Array.isArray(value)) value = value.join(",");
          return `${key}=${value}`;
        })
        .join("&");

      url += "/?" + stringFilters;
    }

    const response = await fetch(url);
    return await response.json();
  }

  public static async getCharacter(id: string): Promise<ICharacter> {
    const url = this.RICK_API_CHARACTERS_URL + "/" + id;

    const response = await fetch(url);
    return await response.json();
  }
}
