import Vue from "vue";
import { RouteManager } from "./route-manager/route-manager";
import { Routes } from "./route-manager/route-manager.types";

interface IRouteConverterOptions<Role, IMenuItemDispatch> {
  ROUTES: Routes<Role, IMenuItemDispatch>;
  REDIRECTIONS: Routes<Role, IMenuItemDispatch>;
}

const routeConverter = {
  install<Role, IMenuItemDispatch>(
    vue: typeof Vue,
    { ROUTES, REDIRECTIONS }: IRouteConverterOptions<Role, IMenuItemDispatch>
  ) {
    const routeManager = new RouteManager<Role, IMenuItemDispatch>(
      ROUTES,
      REDIRECTIONS
    );
    vue.prototype.$routeManager = routeManager;
    vue.$routeManager = routeManager;
  },
};

export default routeConverter;
