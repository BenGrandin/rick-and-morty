/* eslint-disable @typescript-eslint/naming-convention */
// Vue import needed
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { RouteConfig } from "vue-router";
import { IHeaderMenuItem, Routes } from "../route-manager/route-manager.types";

declare module "vue/types/vue" {
  interface Vue {
    $routeManager: IRouteManager;
  }

  interface VueConstructor {
    $routeManager: IRouteManager;
  }

  interface IRouteManager {
    redirections?: Routes<unknown, unknown>;
    routes: Routes<unknown, unknown>;
    getRoutesConfig: () => RouteConfig[];

    getRoutesConfigWithRedirect(): RouteConfig[];

    convertRouteObjectToRouteConfig(
      obj: IHeaderMenuItem<unknown, unknown>
    ): RouteConfig;
  }
}
