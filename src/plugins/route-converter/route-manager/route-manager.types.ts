import { Component } from "vue";
import { RouteConfig } from "vue-router";

type Routes<Role, IMenuItemDispatch> = Record<
  string,
  IHeaderMenuItem<Role, IMenuItemDispatch>
>;

interface IRouteManager<Role, IMenuItemDispatch> {
  redirections?: Routes<Role, IMenuItemDispatch>;
  routes: Routes<Role, IMenuItemDispatch>;
  getRoutesConfig: () => RouteConfig[];

  getRoutesConfigWithRedirect(): RouteConfig[];

  convertRouteObjectToRouteConfig(
    obj: IHeaderMenuItem<Role, IMenuItemDispatch>
  ): RouteConfig;
}

interface IHeaderMenuItem<Role, IMenuItemDispatch>
  extends Omit<RouteConfig, "children"> {
  meta?: {
    title?: string;
    icon?: string;
    roles: Role[];
    visible: {
      appBar?: boolean;
    };
    dispatches?: IMenuItemDispatch[];
    [key: string | number | symbol]: unknown;
  };
  children?:
    | Record<string, IHeaderMenuItem<Role, IMenuItemDispatch>>
    | RouteConfig[];
  component?: Component;
  components?: Record<string, Component>;
}

export { IRouteManager, IHeaderMenuItem, Routes };
