import { RouteConfig } from "vue-router";
import { IHeaderMenuItem, IRouteManager, Routes } from "./route-manager.types";

export class RouteManager<Role, IMenuItemDispatch>
  implements IRouteManager<Role, IMenuItemDispatch>
{
  public constructor(
    public readonly routes: Routes<Role, IMenuItemDispatch>,
    public readonly redirections: Routes<Role, IMenuItemDispatch> = {}
  ) {}

  public getRoutesConfig() {
    return [
      ...Object.values(this.routes).map((x) =>
        this.convertRouteObjectToRouteConfig(x)
      ),
    ];
  }

  public getRoutesConfigWithRedirect() {
    return [
      ...Object.values(this.routes).map((x) =>
        this.convertRouteObjectToRouteConfig(x)
      ),
      ...Object.values(this.redirections).map((x) =>
        this.convertRouteObjectToRouteConfig(x)
      ),
    ];
  }

  // Should this be static / protected ?
  public convertRouteObjectToRouteConfig(
    obj: IHeaderMenuItem<Role, IMenuItemDispatch>
  ) {
    return Object.entries(obj).reduce(
      (acc: Record<string, unknown>, [key, val]) => {
        if (key === "children") {
          acc[key] = Object.values(val).map((x) =>
            this.convertRouteObjectToRouteConfig(
              x as IHeaderMenuItem<Role, IMenuItemDispatch>
            )
          );
        } else {
          acc[key] = val;
        }

        return acc;
      },
      {}
    ) as unknown as RouteConfig;
  }
}
