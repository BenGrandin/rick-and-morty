import NotFoundPage from "@/pages/NotFoundPage.vue";
import HomePage from "@/pages/HomePage.vue";
import { Routes } from "@/plugins/route-converter/route-manager/route-manager.types";
import CharactersPage from "@/pages/CharactersPage.vue";
import CharacterPage from "@/pages/CharacterPage.vue";

interface IMenuItemDispatch {
  path: string;
  payload?: unknown;
}

export enum ERole {
  SEE_HOME = "home",
  SEE_CHARACTER = "character",
}

export enum ERoutes {
  HOME = "home",
  CHARACTERS = "characters",
  CHARACTER = "character",
}

const REDIRECTIONS: Routes<ERole, IMenuItemDispatch> = {
  index: {
    path: "/",
    redirect: {
      name: ERoutes.HOME,
    },
  },
  all: {
    path: "*",
    redirect: {
      name: "not-found",
    },
  },
};

const ROUTES: Routes<ERole, IMenuItemDispatch> = {
  home: {
    name: ERoutes.HOME,
    path: "/" + ERoutes.HOME,
    component: HomePage,
    meta: {
      title: "Accueil",
      icon: "home",
      roles: [ERole.SEE_HOME],
      visible: { appBar: true },
    },
  },
  characters: {
    name: ERoutes.CHARACTERS,
    path: "/" + ERoutes.CHARACTERS,
    component: CharactersPage,
    meta: {
      title: "Détail",
      // TODO check icon
      icon: "avatar",
      roles: [ERole.SEE_CHARACTER],
      visible: { appBar: false },
    },
  },
  character: {
    path: "/" + ERoutes.CHARACTERS + "/:id",
    name: ERoutes.CHARACTER,
    component: CharacterPage,
    props: true,
    meta: {
      roles: [],
      visible: {},
    },
  },
  notFound: {
    name: "not-found",
    path: "/not-found",
    component: NotFoundPage,
    meta: {
      roles: [],
      visible: {},
    },
  },
};

export { ROUTES, REDIRECTIONS, IMenuItemDispatch };
