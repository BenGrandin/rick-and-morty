import Vue from "vue";
import VueRouter from "vue-router";
import routeConverter from "@/plugins/route-converter/route-converter";
import { REDIRECTIONS, ROUTES } from "@/router/routes";

Vue.use(routeConverter, { ROUTES, REDIRECTIONS });

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: Vue.$routeManager.getRoutesConfigWithRedirect(),
  base: process.env.BASE_URL,
});

export default router;
