import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import store from "@/store";
import { CharacterApi, ICharacterFilters } from "@/api/character.api";
import { ICharacter } from "@/api/character.type";
import { IRickApiResponse } from "@/api/rick-api.type";

@Module({
  dynamic: true,
  namespaced: true,
  name: "characterModule",
  store,
})
export class CharacterModule extends VuexModule {
  public errorMessage = "";
  public characters?: IRickApiResponse<ICharacter> = {
    info: {
      count: undefined,
      pages: undefined,
      prev: undefined,
      next: undefined,
      current: 1,
    },
    results: [],
  };
  public isCharactersLoading = false;
  public isCharacterLoading = false;
  public character: ICharacter = {
    created: "",
    episode: [],
    gender: "",
    id: 0,
    image: "",
    location: { name: "", url: "" },
    origin: { name: "", url: "" },
    name: "",
    species: "",
    status: "",
    type: "",
    url: "",
  };

  @Mutation
  private startLoadingCharacters() {
    this.isCharactersLoading = true;
  }

  @Mutation
  private fetchCharactersSuccess({
    characterResponse,
    page = 1,
  }: {
    characterResponse: IRickApiResponse<ICharacter>;
    page?: number;
  }) {
    this.characters = characterResponse;
    this.characters.info.current = page;

    this.errorMessage = "";
    this.isCharactersLoading = false;
  }

  @Mutation
  private failLoadingCharacters(error: Error) {
    this.errorMessage = error.message;
    this.isCharactersLoading = false;
  }

  @Action
  public async getAllCharacters(filters: Partial<ICharacterFilters>) {
    this.startLoadingCharacter();

    try {
      const characterResponse = await CharacterApi.getAllCharacters(filters);

      this.fetchCharactersSuccess({ characterResponse, page: filters.page });
    } catch (e) {
      this.failLoadingCharacter(e as Error);
    }
  }

  @Mutation
  private startLoadingCharacter() {
    this.isCharacterLoading = true;
  }

  @Mutation
  private fetchCharacterSuccess({
    characterResponse,
  }: {
    characterResponse: ICharacter;
  }) {
    this.character = characterResponse;

    this.errorMessage = "";
    this.isCharacterLoading = false;
  }

  @Mutation
  private failLoadingCharacter(error: Error) {
    this.errorMessage = error.message;
    this.isCharacterLoading = false;
  }

  @Action
  public async getCharacter(id: string) {
    this.startLoadingCharacter();

    try {
      const characterResponse = await CharacterApi.getCharacter(id);

      this.fetchCharacterSuccess({ characterResponse });
    } catch (e) {
      this.failLoadingCharacter(e as Error);
    }
  }
}
